#include "trie.h"

#include "alphabet.h"

#include <iostream>
#include <stdexcept>
#include <string>

using Decoder = Trie<Binary, int>;

Decoder createDecoder() {
    Decoder decoder;
    decoder.insert("0", 0);
    decoder.insert("1", 1);
    decoder.insert("10", 2);
    decoder.insert("11", 3);
    decoder.insert("100", 4);
    decoder.insert("101", 5);
    decoder.insert("111", 6);
    return decoder;
}


int main() {
    try {
        Decoder decoder = createDecoder();
        Decoder copyConst(decoder);
        Decoder copyOp = decoder;

        std::string word = "0";
        const int* letter = decoder.search(word);
        std::string word2 = "0";
        int letter2 = decoder.at(word2);
        std::cout << *letter;
        std::cout << '\n';
        std::cout << letter2;
        std::cout << '\n';
//        decoder.items();
        decoder.remove("0");

//        decoder.draw(std::cout);
//        std::cout << "-------------\n";
//        decoder.remove("cat");
//        decoder.draw(std::cout);
//        decoder.clear();
        if(decoder.empty()){
            std::cout << "Empty\n";
        } else {
            std::cout << "not Empty\n";
        }
    } catch (std::out_of_range& ex) {
        std::cerr << "Error: wrong symbol.\n";
    }
}