#include "trie.h"

#include "alphabet.h"

#include <iostream>
#include <stdexcept>
#include <string>

using Decoder = Trie<AlphaNumeric, int>;

Decoder createDecoder() {
    Decoder decoder;
    decoder.insert("a", -7);
    decoder.insert("ale", 14);
    decoder.insert("ape", 21);
    decoder.insert("at", 5);
    decoder.insert("cat", 100);
    decoder.insert("cast", 20);
//    decoder.insert("c", 8);
    return decoder;
}

template class Trie<AlphaNumeric, int>;

int main() {

        Decoder decoder = createDecoder();
        const Decoder copyConst(decoder);
        Decoder copyOp;
        copyOp = decoder;

        std::string word = "ale";
        const int* letter = decoder.search(word);
        std::string word2 = "cat";
        int letter2 = decoder.at(word2);
        std::cout << "Normal constructor\n";
        std::cout << *letter << '\n';
        std::cout << letter2 << '\n';
        decoder.insert("bell", 55);
        decoder.draw(std::cout);

        std::cout << "-------------\n";

        std::cout << "Copy constructor\n";
        letter = copyConst.search(word);
        letter2 = copyConst.at(word2);
        std::cout << *letter << '\n';
        std::cout << letter2 << '\n';
//        copyConst.insert("door", 66);
        copyConst.draw(std::cout);

        std::cout << "-------------\n";

        std::cout << "Assign operator\n";
        letter = copyOp.search(word);
        letter2 = copyOp.at(word2);
        std::cout << *letter << '\n';
        std::cout << letter2 << '\n';
        copyOp.insert("fail", 77);
        copyOp.draw(std::cout);


        int val = decoder["bell"];
        std::cout << val << "\n";
        val = decoder["bella"];
        std::cout << val << "\n";

        auto& root = decoder.root();
        auto* child = root.child('a');
        auto* parent = child->parent();
        std::cout << *child->value() << "\n";
        if(parent->parent()== nullptr){
            std::cout << "OK\n";
        }
        child = child->child('l');
        parent = child;

}