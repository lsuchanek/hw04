#include "trie.h"

#include "alphabet.h"

#include <iostream>
#include <stdexcept>
#include <string>

using Decoder = Trie<MorseCode, char>;

Decoder createDecoder() {
	Decoder decoder;
	decoder.insert(".-",   'A');
	decoder.insert("-...", 'B');
	decoder.insert("-.-.", 'C');
	decoder.insert("-..",  'D');
	decoder.insert(".",    'E');
	decoder.insert("..-.", 'F');
	decoder.insert("--.",  'G');
	decoder.insert("....", 'H');
	decoder.insert("..",   'I');
	decoder.insert(".---", 'J');
	decoder.insert("-.-",  'K');
	decoder.insert(".-..", 'L');
	decoder.insert("--",   'M');
	decoder.insert("-.",   'N');
	decoder.insert("---",  'O');
	decoder.insert(".--.", 'P');
	decoder.insert("--.-", 'Q');
	decoder.insert(".-.",  'R');
	decoder.insert("...",  'S');
	decoder.insert("-",    'T');
	decoder.insert("..-",  'U');
	decoder.insert("...-", 'V');
	decoder.insert(".--",  'W');
	decoder.insert("-..-", 'X');
	decoder.insert("-.--", 'Y');
	decoder.insert("--..", 'Z');
	return decoder;
}

template class Trie<MorseCode, char >;

int main() {

	try {
		Decoder decoder = createDecoder();
//		auto node = decoder.root();
//
//		if(!decoder.empty()){
//			std::cout << "OK\n";
//		}
		std::string word = "--..";
		const char* letter = decoder.search(word);
		std::string word2 = ".-";
		char letter2 = decoder.at(word2);
		std::cout << *letter;
		std::cout << '\n';
		std::cout << letter2;
		std::cout << '\n';
		if(decoder.insert("--..", 'Z')){
			std::cout << "NOK\n";
		} else{
			std::cout << "OK\n";
		}

		char letter3 = decoder["..."];
		std::cout << letter3 << "\n";
		decoder.draw(std::cout);
		std::cout << "-------------------\n";
		decoder.remove("....");
		decoder.draw(std::cout);
		decoder.clear();
	} catch (std::out_of_range& ex) {
		std::cerr << "Error: wrong symbol.\n";
	}
}
