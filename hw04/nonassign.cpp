#include "trie.h"

#include "alphabet.h"

#include <iostream>
#include <stdexcept>
#include <string>


class NonAssignValue{
public:
    int value;
public:
    NonAssignValue(int val){
        value = val;
    }

    friend std::ostream& operator<<(std::ostream& ostream, const NonAssignValue& val){
        ostream << val.value;
        return ostream;
    }

    NonAssignValue& operator=(NonAssignValue& v) = delete;
};

using Decoder = Trie<AlphaNumeric, NonAssignValue>;

Decoder createDecoder() {
    Decoder decoder;
    decoder.insert("a", -7);
    decoder.insert("ale", 14);
    decoder.insert("ape", 21);
    decoder.insert("at", 5);
    decoder.insert("cat", 100);
    decoder.insert("cast", 20);
    decoder.insert("bad",NULL);
    return decoder;
}


int main() {
    try {
        Decoder decoder = createDecoder();
        const Decoder copyConst(decoder);
        Decoder copyOp = decoder;

        std::string word = "a";
        const NonAssignValue* letter = decoder.search(word);
        std::string word2 = "ale";
        NonAssignValue letter2 = decoder.at(word2);
        std::cout << letter->value;
        std::cout << '\n';
        std::cout << letter2.value;
        std::cout << '\n';

        decoder.draw(std::cout);

        decoder.draw(std::cout);
//        decoder.clear();
        auto& root = decoder.root();
        auto* child = root.child('a');
        std::cout << *child->value();

    } catch (std::out_of_range& ex) {
        std::cerr << "Error: wrong symbol.\n";
    }
}