#include "trie.h"

#include "catch.hpp"
#include "alphabet.h"

#include <stdexcept>
#include <iostream>
#include <sstream>

TEST_CASE("Morse"){
    using Decoder = Trie<MorseCode, char>;
    Decoder decoder;
    decoder.insert(".-",   'A');
    decoder.insert("-...", 'B');
    decoder.insert("-.-.", 'C');

    SECTION("insert"){
        CHECK(!decoder.insert(".-", 'A'));
        decoder.insert("...", 'S');
        CHECK(*decoder.search("...") == 'S');
    }

    SECTION("Empty"){
        Decoder decoder1;
        CHECK(decoder1.empty());
        decoder1.insert(".", 'E');
        CHECK_FALSE(decoder1.empty());
        decoder1.remove(".");
        CHECK(decoder1.empty());
    }

    SECTION("search"){

        CHECK(decoder.search(".-") != nullptr);
        CHECK(*decoder.search(".-") == 'A');
        CHECK(decoder.search("-...") != nullptr);
        CHECK(*decoder.search("-...") == 'B');
        REQUIRE_THROWS_AS(decoder.search(".-!"), std::out_of_range);
        REQUIRE_THROWS_AS(decoder.search(","), std::out_of_range);
        CHECK(decoder.search("-...--!") == nullptr);
        CHECK(decoder.search("-.") == nullptr);
    }

    SECTION("at"){
        CHECK(decoder.at(".-") == 'A');
        CHECK(decoder.at("-...") == 'B');
        REQUIRE_THROWS_AS(decoder.at(".-!"), std::out_of_range);
        REQUIRE_THROWS_AS(decoder.at("."), std::out_of_range);
    }

    SECTION("remove"){

        decoder.remove("-.-.");
        CHECK(decoder.search("-.-.") == nullptr);
        CHECK(*decoder.search("-...") == 'B');

    }

    SECTION("operator []"){

        CHECK(decoder[".-"] == 'A');
        CHECK(decoder["-..."] == 'B');
        CHECK(decoder["-.-"] == '\0');
        CHECK(decoder.at("-.-") == '\0');
    }

    SECTION("clear"){

        CHECK(decoder.at(".-") == 'A');
        CHECK(decoder.at("-...") == 'B');
        CHECK(decoder.at("-.-.") == 'C');
        decoder.clear();
        REQUIRE_THROWS_AS(decoder.at(".-"), std::out_of_range);
        REQUIRE_THROWS_AS(decoder.at("-..."), std::out_of_range);
        REQUIRE_THROWS_AS(decoder.at("-.-."), std::out_of_range);
        CHECK(decoder.empty());
    }

    SECTION("items"){
        auto list = decoder.items();
        CHECK(list.size() == 3);
        CHECK(list.at(0).first == ".-");
        CHECK(list.at(0).second == 'A');
    }

    SECTION("draw"){
        decoder.draw(std::cout);
    }
}

TEST_CASE("alphabet"){
    using Decoder = Trie<LowerCaseAlphabet , int>;
    Decoder decoder;
    decoder.insert("a", -7);
    decoder.insert("ale", 14);
    decoder.insert("ape", 21);
    decoder.insert("at", 5);
    decoder.insert("cat", 100);
    decoder.insert("cast", 20);

    SECTION("Empty"){
        Decoder decoder1;
        CHECK(decoder1.empty());
        decoder1.insert("a", 1);
        CHECK_FALSE(decoder1.empty());
        decoder1.remove("a");
        CHECK(decoder1.empty());
    }

    SECTION("search"){

        CHECK(decoder.search("a") != nullptr);
        CHECK(*decoder.search("a") == -7);
        CHECK(decoder.search("ale") != nullptr);
        CHECK(*decoder.search("ale") == 14);
        REQUIRE_THROWS_AS(decoder.search("cat!"), std::out_of_range);
        REQUIRE_THROWS_AS(decoder.search(","), std::out_of_range);
        CHECK(decoder.search("catapult!") == nullptr);
        CHECK(decoder.search("ca") == nullptr);
    }

    SECTION("at"){
        CHECK(decoder.at("a") == -7);
        CHECK(decoder.at("ale") == 14);
        REQUIRE_THROWS_AS(decoder.at("cat!"), std::out_of_range);
        REQUIRE_THROWS_AS(decoder.at("ca"), std::out_of_range);
    }

    SECTION("remove"){

        decoder.remove("ale");
        CHECK(decoder.search("ale") == nullptr);
        CHECK(*decoder.search("ape") == 21);

    }

    SECTION("operator []"){
        CHECK(decoder["a"] == -7);
        CHECK(decoder["ale"] == 14);
        CHECK(decoder["cat"] == 100);
    }

    SECTION("clear"){

        CHECK(decoder.at("a") == -7);
        CHECK(decoder.at("ale") == 14);

        decoder.clear();
        REQUIRE_THROWS_AS(decoder.at("ale"), std::out_of_range);
        REQUIRE_THROWS_AS(decoder.at("cat"), std::out_of_range);
        REQUIRE_THROWS_AS(decoder.at("a"), std::out_of_range);
        CHECK(decoder.empty());
    }

    SECTION("items"){
        auto list = decoder.items();
        CHECK(list.size() == 6);
        CHECK(list.at(0).first == "a");
        CHECK(list.at(0).second == -7);
        CHECK(list.at(1).first == "ale");
        CHECK(list.at(1).second == 14);
    }

    SECTION("draw"){
        decoder.draw(std::cout);
    }

    SECTION("Node tests"){
        auto& root = decoder.root();
        CHECK(root.parent() == nullptr);
        CHECK(root.child('a') != nullptr);
        REQUIRE_THROWS_AS(root.child('.'), std::out_of_range);
        auto* child = root.child('a');
        CHECK(child->parent() == &root);
    }
}

TEST_CASE("String key"){
    using Decoder = Trie<LowerCaseAlphabet , std::string>;
    Decoder decoder;
    decoder.insert("a", "a");
    decoder.insert("ale", "ale");
    decoder.insert("ape", "ape");
    decoder.insert("at", "at");
    decoder.insert("cat", "cat");
    decoder.insert("cast", "cast");


    SECTION("Empty"){
        Decoder decoder1;
        CHECK(decoder1.empty());
        decoder1.insert("a", "a");
        CHECK_FALSE(decoder1.empty());
        decoder1.remove("a");
        CHECK(decoder1.empty());
    }

    SECTION("search"){

        CHECK(decoder.search("a") != nullptr);
        CHECK(*decoder.search("a") == "a");
        CHECK(decoder.search("ale") != nullptr);
        CHECK(*decoder.search("ale") == "ale");
        REQUIRE_THROWS_AS(decoder.search("cat!"), std::out_of_range);
        REQUIRE_THROWS_AS(decoder.search(","), std::out_of_range);
        CHECK(decoder.search("catapult!") == nullptr);
        CHECK(decoder.search("ca") == nullptr);
    }

    SECTION("Node tests"){
        auto& root = decoder.root();
        CHECK(root.parent() == nullptr);
        CHECK(root.child('a') != nullptr);
        REQUIRE_THROWS_AS(root.child('.'), std::out_of_range);
        auto* child = root.child('a');
        CHECK(child->parent() == &root);
    }
}

TEST_CASE("Non assign value"){
    class NonAssignValue{
    public:
        int value;
    public:
        NonAssignValue(int val){
            value = val;
        }

        NonAssignValue(const NonAssignValue& v){
            value = v.value;
        }

        NonAssignValue& operator=(NonAssignValue& v) = delete;
    };

    using Decoder = Trie<AlphaNumeric, NonAssignValue>;

    Decoder decoder;
    decoder.insert("a", -7);
    decoder.insert("ale", 14);
    decoder.insert("ape", 21);
    decoder.insert("at", 5);
    decoder.insert("cat", 100);
    decoder.insert("cast", 20);


    SECTION("Empty"){
        Decoder decoder1;
        CHECK(decoder1.empty());
        decoder1.insert("a", -7);
        CHECK_FALSE(decoder1.empty());
        decoder1.remove("a");
        CHECK(decoder1.empty());
    }

    SECTION("search"){

        CHECK(decoder.search("a") != nullptr);
//        CHECK(*decoder.search("a") == -7);
        CHECK(decoder.search("ale") != nullptr);
//        CHECK(*decoder.search("ale") == 14);
        REQUIRE_THROWS_AS(decoder.search("cat!"), std::out_of_range);
        REQUIRE_THROWS_AS(decoder.search(","), std::out_of_range);
        CHECK(decoder.search("catapult!") == nullptr);
        CHECK(decoder.search("ca") == nullptr);
    }

    SECTION("Node tests"){
        auto& root = decoder.root();
        CHECK(root.parent() == nullptr);
        CHECK(root.child('a') != nullptr);
        REQUIRE_THROWS_AS(root.child('.'), std::out_of_range);
        auto* child = root.child('a');
        CHECK(child->parent() == &root);
    }

}

TEST_CASE("vector as value"){
    using Decoder = Trie<LowerCaseAlphabet , std::vector<int>>;
    Decoder decoder;
    decoder.insert("a", {1,2});

}

