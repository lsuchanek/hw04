#ifndef TRIE_H
#define TRIE_H
#pragma once

#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>
#include <map>
#include <optional>
#include "alphabet.h"

int nodeCount = 0;

template <typename Alphabet, typename Value>
class Trie {

public:
	using ItemList = std::vector<std::pair<std::string, const Value&>>;
	class Node {
        friend class Trie;
    private:
	    int id;
	    Node *parent_;
	    char way;
	    std::vector<std::unique_ptr<Node>> children;
        std::optional<Value> value_;
	    bool hasValue;


	public:

        Node(Node* parent):parent_(parent)
	    {
	        way = '\0';
	        id = nodeCount;
	        nodeCount++;
            children.resize(Alphabet::size);
            hasValue = false;
	    }
		const Node* child(char letter) const {
            int index = Alphabet::ord(letter);
	        if(index == -1){
	            throw std::out_of_range("letter not in alphabet");
	        }
            return children.at(static_cast<unsigned>(index)).get();
	    }

	    const Node* parent() const{
            return parent_;
	    }
		const Value* value() const{
            if(hasValue)
                return &value_.value();
            else
                return nullptr;
	    }

	};

private:
    static Node* findValue(Node* root, const std::string& key) {
        if(!root || key.empty()){
            return nullptr;
        }
        Node* actual = root;
        for(size_t i = 0; i<key.size(); i++){

            char c = key.at(i);
            int index = Alphabet::ord(c);
            if(index == -1){
                throw std::out_of_range("Key not in alphabet");
            }
            if(!actual->children[index])
                return nullptr;
            actual = actual->children.at(index).get();
        }
        if(!actual){
            return nullptr;
        }
        if(!actual->hasValue)
            return nullptr;
        return actual;
    }

    int childNum(Node* node) const {
        int i = 0;
        for(int y = 0; y < Alphabet::size; y++){
            if(node->children[y]){
                ++i;
            }
        }
        return i;
    }

    void addWord(Node* node, ItemList& list, std::string word) const {
        if(node->hasValue){
            std::pair<std::string,  const Value&> p(word, node->value_.value());
            list.push_back(p);
        }
        for(int i = 0; i < Alphabet::size; i++){
            if(node->children[i])
                addWord(node->children.at(i).get(), list, word + Alphabet::chr(i));
        }

    }

    void drawNode(std::ostream& ostream, Node* node) const {
        if(!node->hasValue){
            ostream << "\"" << node->id << "\" [label=\"\"]\n";
        } else {
            ostream << "\"" << node->id << "\" [label=\"" << node->value_.value() << "\"]\n";
        }
        for(int i = 0; i < Alphabet::size; i++){
            if(node->children[i]){
                drawLine(ostream, node, node->children.at(i).get(), Alphabet::chr(i));
                drawNode(ostream, node->children.at(i).get());
            }
        }
    }

    void drawLine(std::ostream& ostream, Node* first, Node* second, char way) const {
        ostream << "\"" << first->id << "\" -> \"" << second->id << "\" [label=\"" << way << "\"]\n";
    }

    std::unique_ptr<Node> root_;
public:

    Trie(){
        root_ = std::make_unique<Node>(nullptr);
    }

    Trie(const Trie& t){
        ItemList items = t.items();
        root_ = std::make_unique<Node>(nullptr);
        for(std::pair<std::string, const Value&> it : items){
            insert(it.first, it.second);
        }
    }

    Trie& operator=(const Trie& t){
        ItemList items = t.items();
        for(std::pair<std::string, const Value&> it : items){
            insert(it.first, it.second);
        }
        return *this;
    }

    const Node& root() const {
        return *root_;
    }

    bool empty() const {
        return childNum(root_.get()) == 0 ;
    }

	const Value* search(const std::string& key) const {
        Node* node = findValue(root_.get(), key);
        if(node){
            if(node->hasValue)
                return &node->value_.value();
            else
                return nullptr;
        } else{
            return nullptr;
        }
    }

    Value* search(const std::string& key) {
        Node* node = findValue(root_.get(), key);
        if(node){
            if(node->hasValue)
                return &node->value_.value();
            else
                return nullptr;
        } else{
            return nullptr;
        }
    }

	Value& at(const std::string& key) {
       Node* node = findValue(root_.get(), key);
       if(node){
           if(node->hasValue)
               return node->value_.value();
           else
               throw  std::out_of_range("");
       }
       throw std::out_of_range("key not found");
    }

    const Value& at(const std::string& key) const {
        Node* node = findValue(root_.get(), key);
        if(node){
            if(node->hasValue)
                return node->value_.value();
            else
                throw  std::out_of_range("");
        }
        throw std::out_of_range("key not found");
    }

	void remove(const std::string& key){
        Node *node = findValue(root_.get(), key);
        bool list = true;
        if(childNum(node) != 0){
            list = false;
        }
        if(!list){
            node->hasValue = false;
        } else {
            node->hasValue = false;
            Node* parent = node->parent_;
            char way = node->way;

            while (parent){
                if(parent->hasValue){
                    parent->children.at(Alphabet::ord(way)).reset();
                    parent->children.at(Alphabet::ord(way)) = nullptr;
                    break;
                }
                if(childNum(parent) > 1){
                    parent->children.at(Alphabet::ord(way)).reset();
                    parent->children.at(Alphabet::ord(way)) = nullptr;
                    break;
                }
                parent->children.at(Alphabet::ord(way)).reset();
                parent->children.at(Alphabet::ord(way)) = nullptr;
                way = parent->way;
                parent = parent->parent_;
            }

        }
    }

    bool insert(std::string key, Value value){
        if(key.empty()){
            return false;
        }
        for(char c : key){

            int index = Alphabet::ord(c);
            if(index == - 1){
                throw std::out_of_range("invalid key");
            }
        }
        Node* actual = root_.get();
        for(char c : key){
            int index = Alphabet::ord(c);
            if(actual->children[index] == nullptr){
                actual->children[index] = std::make_unique<Node>(actual);
                actual->children.at(index).get()->way = Alphabet::chr(index);
            }
            actual = actual->children.at(index).get();
        }
        if(actual->hasValue){
            return false;
        }
        actual->hasValue = true;
        actual->value_.emplace(value);
        return true;
    }

    Value& operator[](const std::string& key){
        Node* node = findValue(root_.get(), key);
        if(node){
            return node->value_.value();
        } else{
            Value value = Value();
            insert(key, value);
            node = findValue(root_.get(), key);
            return node->value_.value();
        }
    }

	 void clear(){
        Node* node = root_.get();
        for(int i = 0; i < Alphabet::size; i++){
            if(node->children[i]){
                node->children.at(i).reset();
                node->children.at(i) = nullptr;
            }
        }
    }

    ItemList items() const {
        ItemList itemList;
        std::string word = "";
        Node* actual = root_.get();
        for(int i = 0; i < Alphabet::size; i++){
            if(actual->children[i])
                addWord(actual->children.at(i).get(), itemList, word + Alphabet::chr(i));
        }
        return itemList;

    }

	void draw(std::ostream& output) const {
        output << "digraph {\n";
        Node* node = root_.get();
        if(!node){
            return;
        }
        drawNode(output, node);
        output << "}\n";
    }
};

#endif