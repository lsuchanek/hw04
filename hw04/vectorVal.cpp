#include "trie.h"

#include "alphabet.h"

#include <iostream>
#include <stdexcept>
#include <string>

using Decoder = Trie<AlphaNumeric, std::vector<int>>;

Decoder createDecoder() {
    Decoder decoder;
    decoder.insert("a", {-7});
    decoder.insert("ale", {4, 5});

    return decoder;
}

template class Trie<AlphaNumeric, std::vector<int>>;

int main() {

    Decoder decoder = createDecoder();
    const Decoder copyConst(decoder);
    Decoder copyOp;
    copyOp = decoder;

    std::string word = "ale";
    auto letter = decoder.search(word);
    for(int i : *letter){
        std::cout << i << " ";
    }

    auto& root = decoder.root();
    auto* child = root.child('a');

}